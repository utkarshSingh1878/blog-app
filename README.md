Please visit :- [Medium Clone App](https://blog-app-pi-ashen.vercel.app/)

# This project is a react powered web application built to mimic the "Medium.com" features.

Click here to know more about this project [Blog App](https://gitlab.com/utkarshSingh1878/blog-app)

```
> git clone https://gitlab.com/utkarshSingh1878/blog-app
> cd blog-app
> npm install
> npm start
```

Tech used:-

1. create-react-app engine
2. material-ui
3. Redux
4. Redux-Toolkit
5. Axios

# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.
