import React from 'react'
import Banner from "../components/Banner"
import Feeds from '../components/Feeds'
import Tags from '../components/Tags'
function Home() {
    return (
        <div className='home' >
            {/* banner */}
            <Banner />
            {/* feeds */}
            <Feeds />
        </div>
    )
}

export default Home