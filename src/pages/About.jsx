
import React, { useEffect, useState } from 'react'
function About() {
    const filePath = "About.md";



    return (
        <div style={{ display: "flex", alignItems: "center", justifyContent: "center", flexDirection: "column", height: "90%", backgroundColor: "#333333" }} >
            <p align="left">
                <a href="https://www.linkedin.com/in/utkarsh-singh-hbase/" target="blank"><img align="center" src="https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/linkedin.svg" alt="Utkarsh singh" height="30" width="40" /></a><a href="https://github.com/CodeNerd-Utkarsh/" target="blank"><img align="center" src="https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/github.svg" alt="CodeNerd-Utkarsh" height="30" width="40" /></a><a href="https://www.hackerrank.com/Utkasrh_Singh" target="blank"><img align="center" src="https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/hackerrank.svg" alt="Utkarsh Singh" height="30" width="40" /></a>

            </p>
            <br />
            <img align="center" src="https://github-readme-stats.vercel.app/api/top-langs?username=CodeNerd-Utkarsh&amp;theme=dark&amp;hide_langs_below=1" alt='' />
            <br />
            <img align="center" src="https://github-readme-stats.vercel.app/api?username=CodeNerd-Utkarsh&amp;show_icons=true&amp;theme=dracula&amp;line_height=27" alt="utkarsh singh" />
        </div>
    )
}

export default About