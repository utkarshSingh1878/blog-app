import React, { useState } from "react";
import "./App.css";

import { BrowserRouter as Router, Routes, Route } from "react-router-dom"
import Home from "./pages/Home"
import Error from "./pages/Error"
import About from "./pages/About";
import Feeds from "./components/Feeds"
import Navbar from "./components/Navbar"
import Article from "./components/Article";
import SignUp from "./components/SignUp";
import SignIn from "./components/SignIn";
import CreatePost from "./components/CreatePost";
import UserFeeds from "./components/UserFeeds";
function App() {
    const [isLoggedIn, setIsLoggedIn] = useState(false);
    const [myInfo, setMyInfo] = useState("")
    return (
        <div className="app">
            <Router>
                {/* navbar */}
                <Navbar isLoggedIn={isLoggedIn} setIsLoggedIn={setIsLoggedIn} myInfo={myInfo} />
                <Routes>
                    <Route path="/" element={<Home />} />
                    <Route path="/about" element={<About />} />
                    <Route path="/feeds" element={<Feeds />} />
                    <Route path="/feeds/feeds/:slug" element={<Article />} />
                    <Route path="/feeds/:slug" element={<Article />} />
                    <Route path="signin/feeds/:slug" element={<Article />} />
                    <Route path="/signup" element={<SignUp />} />
                    <Route path="/signin" element={<SignIn setMyInfo={setMyInfo} setIsLoggedIn={setIsLoggedIn} />} />
                    <Route path="/createPost" element={<CreatePost />} />
                    <Route path="/myFeeds" element={<UserFeeds myInfo={myInfo} />} />
                    <Route path="*" element={<Error />} />
                </Routes>
            </Router>
        </div>
    )
}

export default App;