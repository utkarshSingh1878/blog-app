import React from "react";
import * as RDC from "react-dom/client";
import App from "./App";
import "./index.css"
import { store } from "./redux/store";
import { Provider } from 'react-redux'
const root = document.getElementById("root")
const app = RDC.createRoot(root)

app.render(
    <Provider store={store}>
        <App />
    </Provider>

)
