import React from 'react'
import "./Banner.css"
import MMMDesign from "./MMMDesign"
function Banner() {
    return (
        <div className='banner' >
            <div className="left">
                <div className="heading">
                    Stay Curious.
                </div>
                <div className="slogan">
                    Discover stories, thinking, and expertise from writers on any topic.
                </div>
            </div>
            <div className="right" style={{ height: "100%", width: "100%" }} >
                <MMMDesign />
            </div>
        </div>
    )
}

export default Banner