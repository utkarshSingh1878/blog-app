import { Button, TextField } from '@mui/material';
import axios from 'axios';
import React, { useState } from 'react'
import { Link } from 'react-router-dom';
import SignIn from './SignIn';
import "./SignUp.css";
function SignUp() {
    const [userName, setUserName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [isRegistered, setIsRegistered] = useState(false);
    const [loginInfo, setLoginInfo] = useState("")
    // https://mighty-oasis-08080.herokuapp.com/api//users
    let data = {
        "user": {
            "username": userName,
            "email": email,
            "password": password
        }
    }
    async function resgisterUser(e) {
        e.preventDefault();
        if (!(!/[^a-zA-Z 0-9 @ . ]/.test(email))) {
            alert("Email must be a valid input")
        }
        else {
            const res = await axios({
                method: "post",
                url: "https://mighty-oasis-08080.herokuapp.com/api//users",
                data: data,
                headers: {
                    'Content-Type': "application/json",
                    "Access-Control-Allow-Origin": "*",

                },
            }).then((res) => {
                setLoginInfo(res.data);
                setIsRegistered(true);
                // console.log(loginInfo);
            }).catch((err) => console.log(err))
        }
    }
    return (
        <>
            {
                isRegistered ? (
                    <SignIn />
                ) : (
                    <form
                        onSubmit={resgisterUser}
                        className="signIn" style={{ display: "flex", flexDirection: "column", alignItems: "center", justifyContent: "center", padding: " 5% 10% 0 10%" }} >
                        <h1 className="heading" style={{ fontSize: "44px", lineHeight: "44px", wordSpacing: "10px", fontWeight: "500", color: "#373a3c" }} >
                            Sign Up
                        </h1>
                        <Link className="need" to={"/signin"} style={{ textDecoration: "none", color: "#5cb85c", }} >Already have an account ?</Link>
                        <TextField label="Username" variant="outlined" onChange={(e) => setUserName(e.target.value)} required color={(!/[^a-zA-Z 0-9 @ .]/.test(email)) ? "success" : "warning"} style={{ width: "50%", margin: "2%" }} type="text" />
                        <TextField label="Email" variant="outlined" onChange={(e) => setEmail(e.target.value)} required color={(!/[^a-zA-Z 0-9 @ .]/.test(email)) ? "success" : "warning"} style={{ width: "50%", margin: "2%" }} type="email" />
                        <TextField label="Password" variant="outlined" onChange={(e) => setPassword(e.target.value)} required type={'password'}
                            color={(!/[^a-zA-Z 0-9 . , + , * , ? , ^ , $ , ( , ) , [ , ] , { , } , | , \ ]/.test(password)) ? "success" : "warning"}
                            style={{ width: "50%", margin: "2%" }} />
                        <Button variant="contained" type="submit" color='success' size='large' style={{ marginLeft: "40%" }}  >Sign Up</Button>
                    </form>
                )
            }
        </>
    )
}

export default SignUp