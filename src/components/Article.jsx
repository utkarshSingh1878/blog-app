import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom';
import { MagicSpinner } from 'react-spinners-kit';
import "./Article.css";
function Article() {
    const { slug } = useParams();
    let articleURL = `https://mighty-oasis-08080.herokuapp.com/api//articles/${slug}`
    const [isLoading, setIsLoading] = useState(true)
    const [articleData, setArticleData] = useState("");
    // console.log(articleURL);
    async function fetchSingleArticle() {
        setIsLoading(true)
        const res = await axios.get(articleURL)
        const data = (await res).data;
        setArticleData(data.article);
        setIsLoading(false);
    }
    useEffect(() => {
        fetchSingleArticle();
    }, [])
    console.log(articleData);
    return (
        <>
            {
                isLoading ? (
                    <MagicSpinner size={200} color="#0d7a10" loading={isLoading} />
                ) : (
                    <>
                        <div className="wrapper" style={{ backgroundColor: "#333333", }}>
                            <div className="title">
                                {articleData.title}
                            </div>
                            <div className="top" style={{ marginTop: "25px" }}>
                                <div className="userInfo">
                                    <img src={articleData.author.image} alt="" />
                                    <div className="info" style={{ marginLeft: "10px" }} >
                                        <p style={{ color: "#3d8d3d", fontSize: "16px", fontWeight: "500", lineHeight: "16px" }} >{articleData.author.username}</p>
                                        <span style={{ fontSize: "13px", lineHeight: "16px", color: "#bbbbbb" }} >{`${new Date(articleData.updatedAt).toDateString()}`}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="description">
                            <p>{articleData.description}</p>
                        </div>
                    </>
                )
            }
        </>
    )
}

export default Article