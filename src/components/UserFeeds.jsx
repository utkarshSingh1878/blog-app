import axios from 'axios';
import React, { useEffect, useState } from 'react'
import Articles from './Articles';
import { TraceSpinner } from 'react-spinners-kit'
import { useSelector, useDispatch } from 'react-redux';
import "./UserFeeds.css";
import "./Feeds.css"
import { HiTrendingUp } from 'react-icons/hi';
import Tags from './Tags';
import UserProfile from './UserProfile';
import { Link } from 'react-router-dom';
function UserFeeds({ loginInfo, myInfo }) {
    // https://mighty-oasis-08080.herokuapp.com/api//articles?author=harsh555&limit=5&offset=0
    const { username, email, token } = loginInfo || myInfo;
    const [currUserArticles, setCurrUserArticles] = useState([]);
    const [isLoading, setIsLoading] = useState(false)
    const [articles, setArticles] = useState([])
    const allArticles = useSelector((state) => state.articleReducer)
    async function fetchCurrUserArticles() {
        setIsLoading(true)
        setCurrUserArticles(allArticles)
        console.log(allArticles)
        // await axios.get(`https://mighty-oasis-08080.herokuapp.com/api//articles?author=${username}&limit=5&offset=0`)
        //     .then((res) => {
        //         setCurrUserArticles(res.data.articles)
        //         // console.log(res.data.articles)
        //     }).catch(err => console.log(err))
        setIsLoading(false)
    }
    useEffect(() => {
        fetchCurrUserArticles()
    }, [])
    return (
        <div className="feeds">
            <div className="blogs">
                <div className="heading">
                    <div style={{ color: "#212121", textDecoration: "underline", fontSize: "12px", lineHeight: "16px", fontWeight: "700", borderRadius: "50%" }}  >{
                        isLoading ? (<TraceSpinner />) : (
                            // HiTrendingUp
                            <div className="trending" style={{ display: "flex", alignItems: "center", }} >
                                <HiTrendingUp style={{ borderRadius: "50%", border: "1px solid black", height: "25px", width: "25px" }} />
                                <p style={{ fontSize: "16px", lineHeight: "16px", color: "#292929", fontWeight: "700", marginLeft: "15px" }}>Your Posts</p>
                            </div>
                        )
                    }</div>
                </div>
                {/* <UserProfile loginInfo={loginInfo} /> */}
                <>
                    {/* <img src={userProfile.profile.image} alt="" />
                    <p style={{ textDecoration: "underline" }} >@{userProfile.profile.username}</p> */}
                    <Link className='createNew' to={"/createPost"} ><h2>+</h2></Link>
                </>
                <div className="articles">

                    {
                        currUserArticles.map((article, idx) => (
                            // <Articles key={idx} article={article} />
                            <div key={idx} style={{ borderTop: "1px solid #37a3a2", height: "auto", padding: "5% 0 5% 0", width: "100%" }}>
                                <div className="title" style={{ color: "#333333" }}>
                                    {article.article.title}
                                </div>
                                <div className="desc"  >
                                    {article.article.description}
                                </div>
                            </div>
                        ))
                    }
                </div>
            </div>

        </div>
    )
}

export default UserFeeds