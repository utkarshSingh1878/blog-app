import axios from 'axios';
import React, { useEffect, useState } from 'react'
import "./Tags.css"
import { CubeSpinner } from "react-spinners-kit"
import { Link } from 'react-router-dom';
function Tags({ setSelectedTag }) {
    const URI = "https://mighty-oasis-08080.herokuapp.com/api/tags/"
    const [allTags, setAllTags] = useState([]);
    const [isLoading, setIsLoading] = useState(false)
    async function fetchTags() {
        setIsLoading(true)
        const res = await axios.get(URI);
        // const tags = await res.data();
        // console.log(res.data.tags);
        setAllTags(res.data.tags);
        setIsLoading(false)
    }
    useEffect(() => {
        fetchTags();
    }, [])
    // console.log(allTags);
    return (
        <>
            {
                isLoading ? (
                    <>
                        <CubeSpinner />
                    </>
                ) : (

                    <div className='tags'>
                        <p style={{ textDecoration: "underline", margin: "8px", fontSize: "16px", lineHeight: "16px", color: "#292929", fontWeight: "700", marginLeft: "15px" }} >Popular Tags</p>
                        {
                            allTags.map((tag, idx) => (
                                // <Link>
                                <div className="tag" key={idx} onClick={(e) => setSelectedTag(tag)} >{tag}</div>
                                // </Link>
                            ))
                        }
                    </div>
                )
            }
        </>
    )
}

export default Tags