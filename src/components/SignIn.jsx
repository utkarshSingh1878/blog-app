import { Button, TextField } from '@mui/material'
import axios from 'axios';
import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import Feeds from './Feeds';
import "./SignIn.css"
import UserFeeds from './UserFeeds';
import UserProfile from "./UserProfile"
import { useSelector, useDispatch } from 'react-redux';

function SignIn({ setIsLoggedIn, setMyInfo }) {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [isRegistered, setIsRegistered] = useState(false);
    const [loginInfo, setLoginInfo] = useState("")
    let data = {
        "user": {
            "email": email,
            "password": password
        }
    }
    async function loginUser(e) {
        e.preventDefault();
        if (!(!/[^a-zA-Z 0-9 @ . ]/.test(email))) {
            alert("Email must be a valid input")
        }
        else {
            await axios({
                method: "post",
                url: "https://mighty-oasis-08080.herokuapp.com/api//users/login",
                data: data,
                headers: {
                    'Content-Type': "application/json",
                    "Access-Control-Allow-Origin": "*",
                },
            }).then((res) => {
                setLoginInfo(res.data.user);
                setIsRegistered(true);
                setMyInfo(res.data.user)
                setIsLoggedIn(true)
            }).catch((err) => console.log(err))
        }
    }
    return (
        <>
            {
                isRegistered ? (
                    <>
                        <UserFeeds loginInfo={loginInfo} />
                    </>
                ) : (
                    <form onSubmit={loginUser} className="signIn" style={{ display: "flex", flexDirection: "column", alignItems: "center", justifyContent: "center", padding: " 5% 10% 0 10%" }} >
                        <h1 className="heading" style={{ fontSize: "44px", lineHeight: "44px", wordSpacing: "10px", fontWeight: "500", color: "#373a3c" }} >
                            Sign In
                        </h1>
                        <Link className="need" to={"/signup"} style={{ textDecoration: "none", color: "#5cb85c", }} >Need an account ?</Link>
                        <TextField label="Email" variant="outlined" onChange={(e) => setEmail(e.target.value)} required color={(!/[^a-zA-Z 0-9 @ .]/.test(email)) ? "success" : "warning"} style={{ width: "50%", margin: "2%" }} type={"email"} />
                        <TextField label="Password" variant="outlined" onChange={(e) => setPassword(e.target.value)} required type={'password'}
                            color={(!/[^a-zA-Z 0-9 . , + , * , ? , ^ , $ , ( , ) , [ , ] , { , } , | , \ ]/.test(password)) ? "success" : "warning"}
                            style={{ width: "50%", margin: "2%" }} />
                        <Button variant="contained" type="submit" color='success' size='large' style={{ marginLeft: "40%" }}  >Sign In</Button>
                    </form>
                )
            }
        </>
    )
}

export default SignIn