import React, { useState } from 'react'
import { Link } from 'react-router-dom';
import "./Articles.css"
function Articles({ article }) {
    let slug = article.slug;
    let title = article.title;
    let description = article.description;
    let body = article.body;
    const [updatedAt, setUpdatedAt] = useState(new Date(article.updatedAt).toDateString())
    let userName = article.author.username;
    let userBio = article.author.bio;
    let userImg = article.author.image;
    return (
        <div className='article' >
            <div className="top">
                <div className="userInfo">
                    <img src={userImg} alt="" />
                    <div className="info" style={{ marginLeft: "10px" }} >
                        <p style={{ color: "#3d8d3d", fontSize: "16px", fontWeight: "500", lineHeight: "16px" }} >{userName}</p>
                        <span style={{ fontSize: "13px", lineHeight: "16px", color: "#bbbbbb" }} >{updatedAt}</span>
                    </div>
                </div>
            </div>
            <main>
                <Link style={{ margin: "20px", textDecoration: "none", color: "black" }} to={`feeds/${slug}`}>
                    <div className="title" style={{ color: "#333333" }}>
                        {title}
                    </div>
                    <div className="desc"  >
                        {description}
                    </div>
                </Link>
            </main>
        </div>
    )
}

export default Articles