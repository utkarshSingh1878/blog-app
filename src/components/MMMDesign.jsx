import React from "react";

function Icon() {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            width="585"
            height="462"
            viewBox="0 0 585 462"
            style={{ width: "100%", height: "100%", contentVisibility: "visible" }}
        >
            <defs>
                <clipPath id="__lottie_element_2">
                    <path d="M0 0H585V462H0z"></path>
                </clipPath>
            </defs>
            <g
                strokeLinejoin="round"
                ariaLabel="M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M M"
                clipPath="url(#__lottie_element_2)"
                display="block"
                fontFamily="Shne"
                fontSize="22"
            >
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(-5.364 -187.704) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(41.232 -187.704) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(111.126 -187.704) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(157.722 -187.704) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(204.318 -187.704) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(274.212 -187.704) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(17.934 -160.304) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(41.232 -160.304) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(111.126 -160.304) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(134.424 -160.304) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(157.722 -160.304) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(181.02 -160.304) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(227.616 -160.304) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(274.212 -160.304) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(-261.642 -133.904) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(-215.046 -133.904) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(-168.45 -133.904) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(64.53 -133.904) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(87.828 -133.904) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(111.126 -133.904) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(157.722 -133.904) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(181.02 -133.904) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(227.616 -133.904) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(250.914 -133.904) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(274.212 -133.904) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(64.53 -107.504) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(111.126 -107.504) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(181.02 -107.504) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(274.212 -107.504) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(-145.152 -81.104) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(-28.662 -81.104) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(-5.364 -81.104) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(111.126 -81.104) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(134.424 -81.104) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(204.318 -81.104) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(250.914 -81.104) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(-145.152 -54.704) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(-98.556 -54.704) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(-51.96 -54.704) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(-5.364 -54.704) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(64.53 -54.704) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(111.126 -54.704) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(134.424 -54.704) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(181.02 -54.704) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(64.53 -28.304) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(87.828 -28.304) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(181.02 -28.304) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(204.318 -28.304) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(227.616 -28.304) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(-75.258 -1.904) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(-28.662 -1.904) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(17.934 -1.904) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(87.828 -1.904) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(204.318 -1.904) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(274.212 -1.904) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(-145.152 24.496) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(-98.556 24.496) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(-28.662 24.496) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(64.53 24.496) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(134.424 24.496) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(157.722 24.496) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(181.02 24.496) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(204.318 24.496) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(250.914 24.496) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(-238.344 50.896) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(-75.258 50.896) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(-5.364 50.896) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(181.02 50.896) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(204.318 50.896) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(64.53 77.296) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(111.126 77.296) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(134.424 77.296) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(181.02 77.296) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(17.934 103.696) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(64.53 103.696) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(87.828 103.696) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(134.424 103.696) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(181.02 103.696) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(227.616 103.696) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(-191.748 130.096) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(-168.45 130.096) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(-145.152 130.096) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(-98.556 130.096) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(-51.96 130.096) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(-28.662 130.096) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(17.934 130.096) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(157.722 130.096) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(227.616 130.096) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(64.53 156.496) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(111.126 156.496) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(181.02 156.496) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(227.616 156.496) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(274.212 156.496) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(-98.556 182.896) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(-51.96 182.896) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(64.53 182.896) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(87.828 182.896) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(111.126 182.896) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(134.424 182.896) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(181.02 182.896) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(-28.662 209.296) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(87.828 209.296) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(181.02 209.296) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(227.616 209.296) scale(.22)"
                    ></path>
                </g>
                <g display="inherit">
                    <path
                        d="M46.829 0l22.156-59.518V0h8.963v-72.308h-12.79L43.103-12.891 21.149-72.308H7.956V0h9.064v-59.518L39.075 0h7.754z"
                        display="block"
                        transform="translate(291 232) translate(274.212 209.296) scale(.22)"
                    ></path>
                </g>
            </g>
        </svg>
    );
}

export default Icon;