import { Button, TextareaAutosize, TextField } from '@mui/material'
import axios from 'axios';
import { useSelector, useDispatch } from 'react-redux';
import { addNewArticle } from "../redux/articleSlice"
import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import "./CreatePost.css"
function CreatePost() {
    const [title, setTitle] = useState("");
    const [body, setBody] = useState("");
    const [description, setDescription] = useState("");
    const [tagList, setTagList] = useState([]);
    const dispatch = useDispatch();
    const allArticles = useSelector((state) => state.articleReducer)
    // console.log(allArticles);
    let data = {
        "article": {
            "title": title,
            "description": description,
            "body": body,
            "tagList": tagList,
            "updatedAt": new Date().toDateString()
        }
    }
    async function publishArticle(e) {
        e.preventDefault();
        dispatch(addNewArticle(data))
    }
    return (
        <div>
            <form
                onSubmit={publishArticle}
                className="signIn" style={{ display: "flex", flexDirection: "column", alignItems: "center", justifyContent: "center", padding: " 5% 10% 0 10%" }} >

                <TextField label="Title" variant="outlined" onChange={(e) => setTitle(e.target.value)} required style={{ width: "50%", margin: "2%" }} type="text" />
                <TextField label="What's this article about?" variant="outlined" onChange={(e) => setDescription(e.target.value)} required style={{ width: "50%", margin: "2%" }} type="text" />
                <TextareaAutosize placeholder="Write your article..." minRows={10} variant="outlined" onChange={(e) => setBody(e.target.value)} required type="text"
                    style={{ width: "50%", margin: "2%", padding: "2%", borderRadius: "5px" }} />
                <Button variant="contained" type="submit" color='success' size='large' style={{ marginLeft: "40%" }}  >Publish</Button>
            </form>
        </div>
    )
}

export default CreatePost