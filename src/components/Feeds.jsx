import axios from 'axios'
import React, { useEffect, useState } from 'react'
import "./Feeds.css"
import { HiTrendingUp } from "react-icons/hi"
import Tags from './Tags'
import Articles from './Articles'
import { TraceSpinner } from 'react-spinners-kit'
function Feeds() {
    const [articles, setArticles] = useState([])
    const [isLoading, setIsLoading] = useState(false)
    const [selectedTag, setSelectedTag] = useState("CSS");
    useEffect(() => {
        const URI = `https://mighty-oasis-08080.herokuapp.com/api//articles?tag=${selectedTag}&limit=10&offset=0/`
        async function fetchArticles() {
            setIsLoading(true)
            const res = await axios.get(URI)
            setArticles(res.data.articles)
            setIsLoading(false)
            // console.log(res.data.articles)
        }
        fetchArticles();
    }, [selectedTag])

    return (
        <>
            <div className="feeds">
                <div className="blogs">
                    <div className="heading">
                        <div style={{ color: "#212121", textDecoration: "underline", fontSize: "12px", lineHeight: "16px", fontWeight: "700", borderRadius: "50%" }}  >{
                            isLoading ? (<TraceSpinner />) : (
                                // HiTrendingUp
                                <div className="trending" style={{ display: "flex", alignItems: "center", }} >
                                    <HiTrendingUp style={{ borderRadius: "50%", border: "1px solid black", height: "25px", width: "25px" }} />
                                    <p style={{ fontSize: "16px", lineHeight: "16px", color: "#292929", fontWeight: "700", marginLeft: "15px" }}>Trending on Medium</p>
                                </div>
                            )
                        }</div>
                    </div>
                    <div className="articles">
                        {
                            articles.map((article, idx) => (
                                <Articles key={idx} article={article} />
                            ))
                        }
                    </div>
                </div>
                <div className="tags">
                    <Tags setSelectedTag={setSelectedTag} />
                </div>
            </div>
            {/* <div className="pagination">
                <button>1</button><button>2</button><button>3</button><button>4</button><button>5</button><button>6</button><button>7</button><button>8</button><button>9</button><button>10</button><button>11</button><button>12</button>
            </div> */}
        </>
    )
}
export default Feeds