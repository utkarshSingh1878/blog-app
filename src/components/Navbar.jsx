import React from 'react'
import { Link } from 'react-router-dom'
import "./Navbar.css"
import Logo from "../Medium.svg"
function Navbar({ isLoggedIn, setIsLoggedIn, myInfo }) {
    return (
        <nav className='navbar' >
            <div className="logo">
                <Link to={"/"}>
                    <img src={Logo} alt="" style={{ width: "200px", height: "200px" }} />
                </Link>
            </div>
            <div className="links">
                <Link style={{ margin: "20px", textDecoration: "none", color: "black" }} to={"/feeds"}>Feeds</Link>
                <Link style={{ margin: "20px", textDecoration: "none", color: "black" }} to={"/about"}>About</Link>
                {
                    isLoggedIn ? (
                        <>
                            <Link className='button' style={{ margin: "20px", textDecoration: "none", color: "white", backgroundColor: "#181918", padding: "10px 18px", borderRadius: "35px", }} to={"/myFeeds"}>My Posts</Link>
                            <Link className='button' style={{ margin: "20px", textDecoration: "none", color: "white", backgroundColor: "#181918", padding: "10px 18px", borderRadius: "35px", }} to={"/"}>{myInfo.username}</Link>

                        </>
                    ) : (
                        <>
                            <Link className='button' style={{ margin: "20px", textDecoration: "none", color: "white", backgroundColor: "#181918", padding: "10px 18px", borderRadius: "35px", }} to={"/signup"}>Get Started</Link>
                            <Link className='button' style={{ margin: "20px", textDecoration: "none", color: "white", backgroundColor: "#181918", padding: "10px 18px", borderRadius: "35px", }} to={"/signin"} >Sign In</Link>
                        </>
                    )
                }

            </div>
        </nav>
    )
}

export default Navbar