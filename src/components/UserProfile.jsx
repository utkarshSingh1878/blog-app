import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';
import { SequenceSpinner } from 'react-spinners-kit';
import "./UserProfile.css"
function UserProfile({ loginInfo }) {
    const { username, email, token } = loginInfo;
    const [userProfile, setUserProfile] = useState("");
    const [isLoading, setIsLoading] = useState(true);
    async function fetchUserData() {
        const res = await axios.get(`https://mighty-oasis-08080.herokuapp.com/api//profiles/${username}`)
        const data = res.data;
        // console.log(data)
        setUserProfile(data)
        setIsLoading(false)
    }
    useEffect(() => {
        fetchUserData()
    }, [])
    return (
        <div className="bannerProfile banner"  >
            {
                isLoading ? (
                    <SequenceSpinner />
                ) : (
                    <>
                        <img src={userProfile.profile.image} alt="" />
                        <p style={{ textDecoration: "underline" }} >@{userProfile.profile.username}</p>
                        <Link className='createNew' to={"/createPost"} ><h2>+</h2></Link>
                    </>

                )
            }
        </div>
    )
}

export default UserProfile