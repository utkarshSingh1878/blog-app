import { createSlice } from '@reduxjs/toolkit';

export const articleSlice = createSlice({
    name: "article",
    initialState: [{
        "article": {
            "title": "random title",
            "description": "this is a random article",
            "body": "This is a random title"
        }
    }],
    reducers: {
        addNewArticle: (state, action) => {
            state.push(action.payload)
        },
    },
})

export const { addNewArticle } = articleSlice.actions;
export default articleSlice.reducer;