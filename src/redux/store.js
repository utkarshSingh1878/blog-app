import { configureStore } from "@reduxjs/toolkit";
import articleReducer from "./articleSlice";
import loggedInReducer from "./loggedInSlice"

export const store = configureStore({
    reducer: {
        articleReducer: articleReducer,
        loggedInReducer: loggedInReducer,
    },
})